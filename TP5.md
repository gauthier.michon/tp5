# TP 5 - Une "vraie" topologie ?

## I. Toplogie 1 - intro VLAN

### 2. Setup clients

On vérifie que l'admin1 peut joindre l'admin2 et inversement

<strong> admin1> ping 10.5.10.12 </strong>
84 bytes from 10.5.10.12 icmp_seq=1 ttl=64 time=0.421 ms
84 bytes from 10.5.10.12 icmp_seq=2 ttl=64 time=0.540 ms

<strong> admin2> ping 10.5.10.11 </strong>
84 bytes from 10.5.10.11 icmp_seq=1 ttl=64 time=0.355 ms
84 bytes from 10.5.10.11 icmp_seq=2 ttl=64 time=0.395 ms


On vérifie que le guest1 peut joindre le guest2 et inversement

<strong> guest1> ping 10.5.20.12 </strong>
84 bytes from 10.5.20.12 icmp_seq=1 ttl=64 time=0.375 ms
84 bytes from 10.5.20.12 icmp_seq=2 ttl=64 time=0.597 ms

<strong> guest2> ping 10.5.20.11 </strong>
84 bytes from 10.5.20.11 icmp_seq=1 ttl=64 time=0.269 ms
84 bytes from 10.5.20.11 icmp_seq=2 ttl=64 time=0.360 ms

### 3. Setup VLANs

On vérifie que : 
<strong> Les ports vers admins et guests sont en access </strong>
<code>10   admins                           active    Et0/0</code>
<code>20   guests                           active    Et0/1</code>


On vérifie que :
<strong> Les ports qui relient les deux switch sont en trunk </strong>
<code> Port        Mode             Encapsulation  Status        Native vlan </code>
<code>Et0/2       on               802.1q         trunking      1 </code>



Les admins peuvent toujours se joindre :

<strong>admin1> ping 10.5.10.12</strong>
84 bytes from 10.5.10.12 icmp_seq=1 ttl=64 time=0.444 ms
84 bytes from 10.5.10.12 icmp_seq=2 ttl=64 time=0.549 ms

admin2> ping 10.5.10.11
84 bytes from 10.5.10.11 icmp_seq=1 ttl=64 time=0.456 ms
84 bytes from 10.5.10.11 icmp_seq=2 ttl=64 time=0.513 ms


Les guests peuvent toujours se joindre :

<strong> guest1> ping 10.5.20.12 </strong>
84 bytes from 10.5.20.12 icmp_seq=1 ttl=64 time=0.445 ms
84 bytes from 10.5.20.12 icmp_seq=2 ttl=64 time=0.547 ms

<strong> guest2> ping 10.5.20.11 </strong>
84 bytes from 10.5.20.11 icmp_seq=1 ttl=64 time=0.605 ms
84 bytes from 10.5.20.11 icmp_seq=2 ttl=64 time=0.577 ms


On cherche à vérifier que si un des guests change d'IP vers une IP du réseau admins, il ne peut PAS joindre les autres admins.

On change donc l'ip de l'admin1 en 10.5.20.13 : 

<strong> admin1> ip 10.5.20.13 </strong>
Checking for duplicate address...
PC1 : 10.5.20.13 255.255.255.0


On essaie de ping un guest et on obtient : 

<strong> admin1> ping 10.5.20.11 </strong>
host (10.5.20.11) not reachable


## II. Topologie 2 - VLAN, sous-interface, NAT

### 2. Adressage

On vérifie que les admins et les guests peuvent toujours se ping

<strong> admin1> ping 10.5.10.13 </strong>
84 bytes from 10.5.10.13 icmp_seq=1 ttl=64 time=0.447 ms
84 bytes from 10.5.10.13 icmp_seq=2 ttl=64 time=0.587 ms


<strong> guest1> ping 10.5.20.13 </strong>
84 bytes from 10.5.20.13 icmp_seq=1 ttl=64 time=0.409 ms
84 bytes from 10.5.20.13 icmp_seq=2 ttl=64 time=0.584 ms


### 3. VLAN

On configure le switch3
On active les ports vers les clients sont en access avec : 
<strong> show vlan : </strong>
10   admins                           active    Et0/0
20   guests                           active    Et0/1

On vérifie que les ports qui relient deux switches sont en trunk :
<strong> show interface trunk : </strong>
Port        Mode             Encapsulation  Status        Native vlan
Et0/2       on               802.1q         trunking      1


On configure le switch2 en rajutant les trunk

<code>Port        Mode             Encapsulation  Status        Native vlan </code>
<code>Et0/2       on               802.1q         trunking      1</code>
<code>Et0/3       on               802.1q         trunking      1</code>
<code>Et1/0       on               802.1q         trunking      1</code>


On vérifie que si un guest prend un IP du réseau admins, il ne peut joindre aucune machine.
On change l'adresse ip du guest 1 : 
<strong> guest1> ip 10.5.10.14 10.5.10.254 </strong>
Checking for duplicate address...
PC1 : 10.5.10.14 255.255.255.0 gateway 10.5.10.254

On tente de ping l'admin1 : 
<strong> guest1> ping 10.5.10.11 </strong>
host (10.5.10.11) not reachable

On tente de ping guest1 : 
<strong> guest1> ping 10.5.20.11 </strong>
host (10.5.10.254) not reachable


### 4. Sous-interfaces

On configure les sous interfaces du routeur : 

<code> R1(config)#interface fastethernet0/0.10 </code>
<code> R1(config-subif)#encapsulation dot1Q 10 </code>
<code> R1(config-subif)#ip address 10.5.10.254 255.255.255.0 </code>
<code> R1(config-subif)#exit </code>
<code> R1(config)#interface fastethernet0/0.20 </code>
<code> R1(config-subif)#encapsulation dot1Q 20 </code> 
<code> R1(config-subif)#ip address 10.5.20.254 255.255.255.0 </code>
<code> R1(config-subif)#exit </code>
<code> R1(config)#interface fastethernet0/0 </code>
<code> R1(config-if)#no shut </code>
<code> R1(config-if)#exit </code>


On vérifie que les admins peuvent ping leur gateway

<strong> admin1> ping 10.5.10.254 </strong>
84 bytes from 10.5.10.254 icmp_seq=1 ttl=255 time=9.234 ms
84 bytes from 10.5.10.254 icmp_seq=2 ttl=255 time=5.720 ms

<strong> admin2> ping 10.5.10.254 </strong>
84 bytes from 10.5.10.254 icmp_seq=1 ttl=255 time=9.181 ms
84 bytes from 10.5.10.254 icmp_seq=2 ttl=255 time=7.329 ms

<strong> admin3> ping 10.5.10.254 </strong>
84 bytes from 10.5.10.254 icmp_seq=1 ttl=255 time=9.233 ms
84 bytes from 10.5.10.254 icmp_seq=2 ttl=255 time=6.581 ms


On vérifie que les guests peuvent ping leur gateway

<strong> guest1> ping 10.5.20.254 </strong>
84 bytes from 10.5.20.254 icmp_seq=1 ttl=255 time=9.104 ms
84 bytes from 10.5.20.254 icmp_seq=2 ttl=255 time=8.611 ms

<strong> guest2> ping 10.5.20.254 </strong>
84 bytes from 10.5.20.254 icmp_seq=1 ttl=255 time=9.544 ms
84 bytes from 10.5.20.254 icmp_seq=2 ttl=255 time=6.707 ms

<strong> guest3> ping 10.5.20.254 </strong>
84 bytes from 10.5.20.254 icmp_seq=1 ttl=255 time=9.329 ms
84 bytes from 10.5.20.254 icmp_seq=2 ttl=255 time=9.842 ms


### 5. NAT

On configure le nat sur le routeur.
On commence par faire : 
<code> ip address dhcp </code>
pour récupérer une ip en dhcp

puis, on fait : 

<code> (config)# interface fastEthernet 0/0.10 </code>
<code> (config-if)# ip nat inside </code>
<code> (config-if)# exit </code>

<code> (config)# interface fastEthernet 0/0.20 </code>
<code> (config-if)# ip nat inside </code>
<code> (config-if)# exit </code>

<code> (config)# interface fastEthernet 1/0 </code>
<code> (config-if)# ip nat outside </code>
<code> (config-if)# exit </code>

<code> (config)# access-list 1 permit any </code>

<code> (config)# ip nat inside source list 1 interface fastEthernet 1/0 overload </code>


on vérifie que les admins et guests ont internet en faisant un ping 8.8.8.8 : 


<strong> admin2> ping 8.8.8.8 </strong>
84 bytes from 8.8.8.8 icmp_seq=1 ttl=50 time=29.722 ms
84 bytes from 8.8.8.8 icmp_seq=2 ttl=50 time=28.313 ms

<strong> guest3> ping 8.8.8.8 </strong>
84 bytes from 8.8.8.8 icmp_seq=1 ttl=50 time=39.863 ms
84 bytes from 8.8.8.8 icmp_seq=2 ttl=50 time=38.722 ms


## III. Topologie 3 - Ajouter des services

Après avoir tout configurer, on met une ip dynamiquement à guest3
en faisant ip dhcp, on obtient : 

<strong> guest3> ip dhcp </strong>
DDORA IP 10.5.20.100/24 GW 10.5.20.254